#!/bin/bash

yum -y install centos-release-gluster312
yum -y install glusterfs gluster-cli glusterfs-libs glusterfs-server
yum clean all
cat <<EOS> /etc/hosts
127.0.0.1       localhost localhost.localdomain localhost4 localhost4.localdomain4
192.168.33.11   g1
192.168.33.12   g2
EOS

systemctl enable glusterd.service
systemctl start glusterd.service
